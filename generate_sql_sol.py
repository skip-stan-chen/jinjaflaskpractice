from flask import Flask, json, render_template
from jinja2 import Template as jinja_template
import json

app = Flask(__name__)


@app.route('/')
def generate_sql():
    # Please note that loading in flask is very different than loading json via using package json
    data = json.load(open('input2.json'))
    sql_command = jinja_template('''
{% for d in data %}
SELECT * FROM {{ d['schema'] }}.{{ d['table'] }};<br/>
{% endfor %}
''')
    return sql_command.render(data=data)


# Rendering a template
@app.route('/sql/<schema>/<table_name>')
def render_sql_from_template(schema, table_name):
    return render_template('select_all_from_schema_table.sql', schema=schema, table_name=table_name)
