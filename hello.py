from flask import Flask, json
from jinja2 import Template as jinja_template
import json

app = Flask(__name__)


@app.route('/')
def hello_world():
    # Please note that loading in flask is very different than loading json via using package json
    data = json.load(open('input.json'))
    hello_what = jinja_template('''
{% for d in data %}
We are greeting with {{ d['greeting'] }} 
addressed to {{ d['object'] }} <br/><br/>
{% endfor %}
''')
    return hello_what.render(data=data)