# Minimal Jinja Templating with Flask Crash Course
(To view this Markdown file properly, please open the project in PyCharm. )

Jinja usage may differ from project to project. To make this material relevant, we will setup a simple Flask app since Airflow is built with Flask framework. 

[Official Flask Guide](https://flask.palletsprojects.com/en/1.1.x/quickstart/#a-minimal-application)

## Pre-requisites

```shell script
pip install Flask
```

# Setup Flask
Create hello.py with the following code: 

```python
from flask import Flask
app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello, World!'
```

start the app server

```shell script
$ export FLASK_APP=hello.py
$ flask run
 * Running on http://127.0.0.1:5000/
```

navigate to  http://127.0.0.1:5000/, you should see Hello World!

If the server runs, means the setup is successful, now send ctrl + c to stop the server. 

# Setup Jinja

Install Jinja

```shell script
pip install jinja2
```

In the hello.py, import jinja module, and replace hello.py with the following code:

```python
from flask import Flask
from jinja2 import Template as jinja_template

app = Flask(__name__)

@app.route('/')
def hello_world():
    hello_what = jinja_template("Hello {{ something }}!")
    return hello_what.render(something="World :) !")

```

Navigate to  http://127.0.0.1:5000/

and observe the difference. 

# Using JSON as inputs

in this directory, there's a JSON file named input.json

in this step we'll we write the whole hello.py to recursively print with template

```python
from flask import Flask, json
from jinja2 import Template as jinja_template
import json

app = Flask(__name__)


@app.route('/')
def hello_world():
    # Please note that loading in flask is very different than loading json via using package json
    data = json.load(open('input.json'))
    hello_what = jinja_template('''
{% for d in data %}
We are greeting with {{ d['greeting'] }} 
addressed to {{ d['object'] }} <br/><br/>
{% endfor %}
''')
    return hello_what.render(data=data)
```

as shown above, when the flask app is ran, its output is shown below:

```markdown
set greeting with Echo set to Earth

set greeting with Hi set to Lua

set greeting with Cat set to Mars

```

# Practice
create a flask app named `generate_sql.py` based off `hello.py`. Load `input2.json` and then make the flask output generate following SQL commands:

```markdown
SELECT * FROM customer.purchase;
SELECT * FROM customer.address;
SELECT * FROM customer.number;
```


don't forget to `export FLASK_APP=generate_sql.py` before running the flask app. please try not to look at the solution!


# Emulating actual usage
Now, let last task in this CC emulates some of the processes our usage of Jinja templating. By default, render_template is searching for template files under ./templates: 


```python
# Rendering a template
@app.route('/sql/<schema>/<table_name>')
def render_sql_from_template(schema, table_name):
    return render_template('', '', '')
```

Under the `./templates/`, examine the template. observer the parameter variables that it takes. Finish the following code to enable to app route to print:

```
SELECT * FROM Schema.Table_name
```

When a request is made at: 
http://127.0.0.1:5000/sql/Schema/Table_name

Again, please do not look at the solution prior to attempt.

Stan: 
this is just something quick I put together, it's currently a working progress. please let me know if you see any error or typo. Thanks and I hope you enjoy this quick CC.

